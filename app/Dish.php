<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Dish extends Model  {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'dishes';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','image'];
}
