<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\dish;
use App\Http\Requests\Admin\DishRequest;
use App\Http\Requests\Admin\DishEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Illuminate\Support\Facades\Input;
use App\Helpers\Thumbnail;
use Datatables;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


class DishController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.dishes.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate() {
        return view('admin.dishes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(DishRequest $request) {

        $dish = new Dish ();
        $dish -> name = $request->name;
        $dish -> price = $request->price;
        $dish -> type = $request->type;
        $picture = "";
        if($request->hasFile('image'))
        {
            $log = new Logger('image');
        $log->pushHandler(new StreamHandler('C:\logweb\your.log', Logger::WARNING));

        
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            // add records to the log
        $log->addWarning('co file',['picture'=>$picture]);
        }
        
        $dish -> image = $picture;
        $dish -> save();
        if($request->hasFile('image'))
        {
            $destinationPath = public_path() . '/appfiles/dishimages/';
            $request->file('image')->move($destinationPath, $picture);

            $path2 = public_path() . '/appfiles/dishimages' . '/thumbs/';
            Thumbnail::generate_image_thumbnail($destinationPath . $picture, $path2 . $picture);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $dish
     * @return Response
     */
    public function getEdit($id) {

        $dish = Dish::find($id);

        return view('admin.dishes.edit', compact('dish'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $dish
     * @return Response
     */
    public function postEdit(DishEditRequest $request, $id) {

        $dish = Dish::find($id);
        $dish -> price = $request->price;
         $dish -> type = $request->type;
        $picture = "";
        if($request->hasFile('image'))
        {
            $log = new Logger('image');
        $log->pushHandler(new StreamHandler('C:\logweb\your.log', Logger::WARNING));

        
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            // add records to the log
            $log->addWarning('co file',['picture'=>$picture]);
            $dish -> image = $picture;
        }
        $dish -> save();
        if($request->hasFile('image'))
        {
            $destinationPath = public_path() . '/appfiles/dishimages/';
            $request->file('image')->move($destinationPath, $picture);

            $path2 = public_path() . '/appfiles/dishimages' . '/thumbs/';
            Thumbnail::generate_image_thumbnail($destinationPath . $picture, $path2 . $picture);

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $dish
     * @return Response
     */

    public function getDelete($id)
    {
        $dish = Dish::find($id);
        // Show the page
        return view('admin.dishes.delete', compact('dish'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $dish
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $dish= Dish::find($id);
        $dish->delete();
        return redirect('admin/dishes');
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data()
    {
       
    $dishes = Dish::select(array('dishes.id','dishes.id','dishes.name','dishes.image', 'dishes.price', 'dishes.created_at'))->orderBy('dishes.name', 'ASC');
        //$dishs = dish::select(array('dishs.id','dishs.name','dishs.email', 'usedishesrs.created_at'))->orderBy('dishs.email', 'ASC');

        return Datatables::of($dishes)
            ->add_column('actions', '<a href="{{{ URL::to(\'admin/dishes/\' . $id . \'/edit\' ) }}}" class="btn btn-success btn-sm iframe" ><span class="glyphicon glyphicon-pencil"></span>  {{ Lang::get("admin/modal.edit") }}</a>
                    <a href="{{{ URL::to(\'admin/dishes/\' . $id . \'/delete\' ) }}}" class="btn btn-sm btn-danger iframe"><span class="glyphicon glyphicon-trash"></span> {{ Lang::get("admin/modal.delete") }}</a>
                ')
            ->edit_column('image', '{!! ($image!="")? "<img style=\"max-width: 100px; max-height: 70px;\" src=\"http://localhost:86/abckitchen1/public/appfiles/dishimages/thumbs/$image\">":""; !!}')
            ->add_column('check','<input class="mycheckbox" type="checkbox" name="check[]" value="{{$id}}"/>')
            ->make(true);
    }

    public function dessertsdata()
    {
       
    $dishes = Dish::where('type','=',1)->select(array('dishes.id','dishes.id','dishes.name','dishes.image', 'dishes.price', 'dishes.created_at'))->orderBy('dishes.name', 'ASC');
        //$dishs = dish::select(array('dishs.id','dishs.name','dishs.email', 'usedishesrs.created_at'))->orderBy('dishs.email', 'ASC');

        return Datatables::of($dishes)
            ->edit_column('image', '<img style="max-width: 100px; max-height: 70px;" src="{{URL::to("appfiles/dishimages/thumbs/$image")}}">')
            ->add_column('check','<input type="checkbox" class="mycheckbox" onclick="checkboxChange.call(this)" name="check[]" value="{{$id}}"/>')
            ->make(true);
    }

    public function maindata()
    {
       
    $dishes = Dish::where('type','=',2)->select(array('dishes.id','dishes.id','dishes.name','dishes.image', 'dishes.price', 'dishes.created_at'))->orderBy('dishes.name', 'ASC');
        //$dishs = dish::select(array('dishs.id','dishs.name','dishs.email', 'usedishesrs.created_at'))->orderBy('dishs.email', 'ASC');

        return Datatables::of($dishes)
            ->edit_column('image', '<img style="max-width: 100px; max-height: 70px;" src="{{URL::to("appfiles/dishimages/thumbs/$image")}}">')
            ->add_column('check','<input type="checkbox" class="mycheckbox" onclick="checkboxChange.call(this)"  name="check[]" value="{{$id}}"/>')
            ->make(true);
    }

    public function soupdata()
    {
       
    $dishes = Dish::where('type','=',3)->select(array('dishes.id','dishes.id','dishes.name','dishes.image', 'dishes.price', 'dishes.created_at'))->orderBy('dishes.name', 'ASC');
        //$dishs = dish::select(array('dishs.id','dishs.name','dishs.email', 'usedishesrs.created_at'))->orderBy('dishs.email', 'ASC');

        return Datatables::of($dishes)
            ->edit_column('image', '<img style="max-width: 100px; max-height: 70px;" src="{{URL::to("appfiles/dishimages/thumbs/$image")}}">')
            ->add_column('check','<input type="checkbox" class="mycheckbox" onclick="checkboxChange.call(this)"  name="check[]" value="{{$id}}"/>')
            ->make(true);
    }
   
}
