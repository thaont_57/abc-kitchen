<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\FuelDaily;
use App\Fuel;
use Response;
use App\Http\Requests\Admin\FuelDailyRequest;
use App\Http\Requests\Admin\FuelDailyEditRequest;
use App\Http\Requests\Admin\DeleteRequest;
use Illuminate\Support\Facades\Input;
use Datatables;

class FuelDailyController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.fueldailys.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($date) {
        return view('admin.fueldailys.create',compact('date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(FuelDailyRequest $request) {

        $fueldaily = new FuelDaily();
        $fueldaily -> time = $request->date;
        $fueldaily ->save();
        $names = $request->names;
        $prices = $request->prices;
        $quantitys = $request->quantitys;
        $totals = $request->totals;
        if($names!=null) {
            for($i=0;$i<count($names);$i++) {
                $fuel = new Fuel();
                $fuel->name= $names[$i];
                $fuel->price= $prices[$i];
                $fuel->quantity= $quantitys[$i];
                $fuel->total= $totals[$i];
                $fuel->fueldaily_id=$fueldaily->id;
                $fuel->save();
            }
        }else{
             return Response::json(500 );
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $fueldaily
     * @return Response
     */
    public function getEdit($id) {

        $fueldaily = FuelDaily::find($id);

        return view('admin.fueldailys.edit', compact('fueldaily'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $fueldaily
     * @return Response
     */
    public function postEdit(fueldailyEditRequest $request, $id) {

        $fueldaily = fueldaily::find($id);
        $fueldaily -> price = $request->price;
        $fueldaily->save();
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param $fueldaily
     * @return Response
     */

    public function getDelete($id)
    {
        $fueldaily = fueldaily::find($id);
        // Show the page
        return view('admin.fueldailys.delete', compact('fueldaily'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $fueldaily
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$id)
    {
        $fueldaily= fueldaily::find($id);
        $fueldaily->delete();
    }

    /**
     * Show a list of all the languages posts formatted for Datatables.
     *
     * @return Datatables JSON
     */
    public function data($date)
    {
        $fueldaily = FuelDaily::where("time","=",$date)->first();
        if($fueldaily!=null) {
            $fuels = Fuel::where("fueldaily_id","=",$fueldaily->id)->
            orderBy('fuels.name', 'ASC')->get();
            if( $fuels!=null) {
                return ["fuels"=>$fuels ];
            }
            return Response::json(500 );
            
        }
        return Response::json(500 );
    }

    public function show($id)
    {
        $fueldaily = fueldaily::find($id);
        // Show the page
        return view('fueldailys.index', compact('fueldaily'));
    }

   
}
