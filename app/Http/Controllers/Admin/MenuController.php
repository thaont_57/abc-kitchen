<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\AdminController;
use App\Menu;
use App\User_Menu;
use Auth;
use Validator;
use Response;
use App\Dish;
use App\Assigneddishes;
use App\Http\Requests\Admin\MenuRequest;
// use App\Http\Requests\Admin\MenuEditRequest;
use App\Http\Requests\Admin\DeleteRequest;


class MenuController extends AdminController {

    /*
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        // Show the page
        return view('admin.menus.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate($date) {
        return view('admin.menus.create_edit',compact('date'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postCreate(menuRequest $request) {
        $today = $request->eat_time;
        $nextday = $request->nextday;
        $lastday = $request->lastday;
        $menu1 = Menu::where("eat_time","=",$lastday)->first();
        $menu2 = Menu::where("eat_time","=",$nextday)->first();
        $values = $request->values;
        $check=false;
        if($menu1!=null) {
            foreach ($values as $value) {
                $menu_dish = Assigneddishes::where("menu_id","=",$menu1->id)->where("dish_id","=",$value)->first();
                if($menu_dish==null) {
                    $check=true;
                }
            }
        }
        if($check!=true) {
            if($menu2!=null) {
                foreach ($values as $value) {
                    $menu_dish = Assigneddishes::where("menu_id","=",$menu2->id)->where("dish_id","=",$value)->first();
                    if($menu_dish==null) {
                        $check=true;
                    }
                }
            }
        }
        if($menu1==null){
            if($menu2==null) {
                $check=true;
            }
        }

        if($check==true) {
            $menu = new menu();
            $menu -> eat_time= $request->eat_time;
            $menu -> save();
           $total=0;
            foreach ($values as $value) {
                $menu_dish = new Assigneddishes();
                $menu_dish->menu_id=$menu->id;
                $menu_dish->dish_id=$value;
                $menu_dish->save();
                $dish=Dish::where("id","=",$value)->first();
                $total=$total+$dish->price;
            }
            $menu->price=$total;
            $menu->save();
        } else {
           $messages = [
                'min' => 'Thực Đơn không được trùng với các thực đơn gần đây',
            ];
           $validator = Validator::make(
                ['name' => 'Dayl'],
                ['name' => 'min:5'],
                $messages
            );
            return Response::json($validator->errors()->all(),500 );
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $menu
     * @return Response
     */
    public function getEdit($id) {

        $menu = Menu::find($id);

        return view('admin.menus.create_edit', compact('menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $menu
     * @return Response
     */
    public function postEdit(menuEditRequest $request, $id) {

        $menu = Menu::find($id);
        $menu -> price = $request->price;
        $picture = "";
        if($request->hasFile('image'))
        {
            $log = new Logger('image');
        $log->pushHandler(new StreamHandler('C:\logweb\your.log', Logger::WARNING));

        
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file -> getClientOriginalExtension();
            $picture = sha1($filename . time()) . '.' . $extension;
            // add records to the log
            $log->addWarning('co file',['picture'=>$picture]);
            $menu -> image = $picture;
        }
        $menu -> save();
        if($request->hasFile('image'))
        {
            $destinationPath = public_path() . '/appfiles/menuimages/';
            $request->file('image')->move($destinationPath, $picture);

            $path2 = public_path() . '/appfiles/menuimages' . '/thumbs/';
            Thumbnail::generate_image_thumbnail($destinationPath . $picture, $path2 . $picture);

        }

        return redirect('admin/menus');
    }
    /**
     * Remove the specified resource from storage.
     * show ui delete
     * @param $menu
     * @return Response
     */

    public function getDelete($date)
    {
        $menu = Menu::where("eat_time","=",$date)->first();
        // Show the page
        return view('admin.menus.delete', compact('menu'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $menu
     * @return Response
     */
    public function postDelete(DeleteRequest $request,$date)
    {
        $menu= menu::where("eat_time","=",$date)->first();
        $menu->delete();
        return redirect('admin/menus');
    }

    public function getmenu($date) {
        $resign=false;
        $menu = Menu::where('eat_time','LIKE','%'.$date.'%')->lists('id');
        if(!empty($menu)) {
            $menues = Assigneddishes::join('dishes','dishes.id','=','dishes_menus.dish_id')->where('menu_id',"=",$menu)->get();
            $users = User_Menu::where("user_id","=",Auth::user()->id)->where("menu_id","=",$menu);
            if($users->count()!=0) {
                $resign=true;
            }
            return ["dishes"=>$menues ,"check"=>$resign];
        }
        else
            return '';

    }


}
