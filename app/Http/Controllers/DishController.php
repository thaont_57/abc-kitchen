<?php namespace App\Http\Controllers;
use Auth;
use Input;
use App\User_Menu;
use App\Menu;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class DishController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function showfordate($date)
	{
		return view('dishes.showmenu',compact('date'));
	}

	public function show()
	{
		$date='';
		return view('dishes.showmenu',compact('date'));
	}
	// resign food
	public function resign()
	{
		$date=Input::get('rdate');
		$today=Input::get('todaydate');
		if($date>$today) {
			$resign = new User_Menu();
			$resign-> user_id=Auth::user()->id;
			$resign-> menu_id = Menu::where("eat_time","like",$date)->first()->id;
			$resign->save();
		}else {
			return Response::json(500 );
		}
		
	}

	// cancel resign food
	public function destroyresign()
	{
		$date=Input::get('ddate');
		$today=Input::get('todaydate');
		if($date>$today) {
			$menus = Menu::where("eat_time","like",$date);
			if($menus->count()!=0) {
				$users = User_Menu::where("user_id","=",Auth::user()->id)->where("menu_id","=",$menus->first()->id);
				$users->delete();
			} else {
				return false;
			}
		}else {
			return Response::json(500 );
		}
		
	}
}
