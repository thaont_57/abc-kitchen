<?php namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'eat_time' => 'required|unique:menus',
            'values' => 'required|size:6',
            'nextday' => 'required',
		];
	}
	public function messages() {
		return [
            'eat_time' => 'Ngày Này đã có thực đơn rồi',
            'values.size' => 'Một thực đơn phải có đúng 6 món ăn',
		];
	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

}