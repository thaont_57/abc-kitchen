
$(function(){

  var picker = new Pikaday(
    {
        field: document.getElementById('myCalendar'),
        firstDay: 1,
        format: 'D MMM YYYY',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000, 2020],
        bound: false,
        showWeekNumber: true,
        onSelect: function() {
            window.location.replace("http://localhost:86/abckitchen1/public/resignfood/" + moment(picker.getDate()).format("YYYY-MM-DD"));
            var idate=moment(picker.getDate()).format("YYYY-MM-DD");
        }
    });
});