$(function(){
    var picker = new Pikaday(
    {
        field: document.getElementById('myCalendar'),
        firstDay: 1,
        format: 'D MMM YYYY',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000, 2020],
        bound: false,
        showWeekNumber: true,
        onSelect: function() {
            getMenu();
        }
    });
    picker.setDate(new Date(),0);

    function getMenu() {
    var date=moment(picker.getDate()).format("YYYY-MM-DD");
   
        $("#new").attr("href","menus/create/"+date);
        var link ='http://localhost:86/abckitchen1/public/menus/' + date + '/data';
        $.ajax({
            url : link,
            type : 'GET',
            dataType:'json',
            success : function(data) {
                $("#new").hide();
                var items = [];
                var sum=0;
                $(".menu").html("");
                $( "<h3>" ).attr( "id", "date").html(date).appendTo( ".menu" );
                $( "<br>" ).appendTo( ".menu" );
                $( "<br>" ).appendTo( ".menu" );
                var dishes = data.dishes;
                $.each( dishes, function( i, item ) {
                    sum+=dishes[i].price;
                    items.push( "<div class='row dish'>");
                    items.push( "<div class='col-md-8'><p>"+dishes[i].name+"</p></div>");
                    items.push( "<div class='col-md-4'><p>"+dishes[i].price+" Đ</p></div>");
                    items.push( "</div>");
                  });
                items.push( "<div class='row dish'>");
                items.push( "<div class='col-md-8'><p>Tổng Tiền</p></div>");
                items.push( "<div class='col-md-4'><p>"+sum+" Đ</p></div>");
                items.push( "</div>");
                $( "<div>" ).attr( "id", "listdishes").html(items.join("")).appendTo( ".menu" );
                $( "<button>" ).attr( "type", "button").attr( "id", "submit")
                .attr( "class", "btn btn-success btn-sm iframe").html("Chỉnh Sửa").appendTo( ".menu" );
                $( "<a>" ).attr("href","menus/"+date+"/delete").
                attr( "type", "button").attr( "class", "btn btn-danger btn-sm iframe").html("Xóa").appendTo( ".menu" );
                $(".iframe").colorbox({
        iframe:true, width:"85%", height:"98%",
        onClosed: function () {
            getMenu(); 
        }
    });
                
            },
            error : function(request,error)
            {
                $(".menu").html("");
                $( "<h3>" ).attr( "id", "date").html(date).appendTo( ".menu" );
                $( "<br>" ).appendTo( ".menu" );
                $( "<br>" ).appendTo( ".menu" );
                $( "<h3>" ).attr( "class", "notice").html("Chưa có thực đơn").appendTo( ".menu" );
                $("#new").show();
            }
        });
    }

    $(".iframe").colorbox({
        iframe:true, width:"85%", height:"98%",
        onClosed: function () {
            getMenu(); 
        }
    });
               
        
});

