$(function(){
    var picker = new Pikaday(
    {
        field: document.getElementById('myCalendar'),
        firstDay: 1,
        format: 'D MMM YYYY',
        minDate: new Date('2000-01-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2000, 2020],
        bound: false,
        showWeekNumber: true,
        onSelect: function() {
            getFuel();
        }
    });

    picker.setDate(new Date(),0);

    
    function getFuel() {
        var date=moment(picker.getDate()).format("YYYY-MM-DD");
        var link ='http://localhost:86/abckitchen1/public/admin/fueldailys/' + date + '/data';
        $("#new").attr("href","fueldailys/create/"+date);
        var date=moment(picker.getDate()).format("YYYY-MM-DD");
        $.ajax({
            url : link,
            type : 'GET',
            dataType:'json',
            success : function(data) {
               
                $("#new").hide();

                var items = [];
                var sum=0;
                 $("#list").html("");
                $( "<h3>" ).attr( "id", "date").html(date).appendTo( "#list" );
                $( "<br>" ).appendTo( "#dist" );
                $( "<br>" ).appendTo( "#dist" );
                 items.push( "<div class='row dish'>");
                    items.push( "<div class='col-md-3'><p>Tên Nguyên Liệu</p></div>");
                    items.push( "<div class='col-md-3'><p>Số Lượng</p></div>");
                    items.push( "<div class='col-md-3'><p>Thành Giá</p></div>");
                    items.push( "<div class='col-md-3'><p>Tổng Tiền</p></div>");
                    items.push( "</div>");
                    items.push( "<br>");
                var fuels = data.fuels;
                if(fuels!=null) {
                $.each( fuels, function( i, item ) {
                    sum+=fuels[i].total;
                    items.push( "<div class='row dish'>");
                    items.push( "<div class='col-md-3'><p>"+fuels[i].name+"</p></div>");
                    items.push( "<div class='col-md-3'><p>"+fuels[i].quantity+"</p></div>");
                    items.push( "<div class='col-md-3'><p>"+fuels[i].price+"</p></div>");
                    items.push( "<div class='col-md-3'><p>"+fuels[i].total+"</p></div>");
                    items.push( "</div>");
                  });
                items.push( "<div class='row dish'>");
                items.push( "<div class='col-md-9'><p>Tổng Tiền</p></div>");
                items.push( "<div class='col-md-3'><p>"+sum+" Đ</p></div>");
                items.push( "</div>");
                $( "<div>" ).attr( "id", "listfuels").html(items.join("")).appendTo( "#list" );
                $( "<button>" ).attr( "type", "button").attr( "id", "submit")
                .attr( "class", "btn btn-success btn-sm iframe").html("Chỉnh Sửa").appendTo( "#list" );
                $( "<a>" ).attr("href","fueldailys/"+date+"/delete").
                attr( "type", "button").attr( "class", "btn btn-danger btn-sm iframe").html("Xóa").appendTo( "#list" );
            } else {
                $("#list").html("");
                $( "<br>" ).appendTo( "#list" );
                $( "<br>" ).appendTo( "#list" );
                $( "<h3>" ).html("Chưa Có Đơn Hàng!").appendTo( "#list" );
                $("#new").show();
            }
                
            },
            error : function(request,error)
            {
                $("#list").html("");
                $( "<br>" ).appendTo( "#list" );
                $( "<br>" ).appendTo( "#list" );
                $( "<h3>" ).html("Chưa Có Đơn Hàng!").appendTo( "#list" );
                $("#new").show();
            }
        });
    }
    $(".iframe").colorbox({
        iframe:true, width:"85%", height:"98%",
        onClosed: function () {
            getFuel(); 
        }
    });
               
        
});

