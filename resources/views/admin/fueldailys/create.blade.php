@extends('admin/model')
@section('content')
<ul class="nav nav-tabs">
	<li class="active"><a href="#tab-general" data-toggle="tab">{{{
			Lang::get('admin/modal.general') }}}</a></li>
</ul>



	<div class="tab-content">
		<div class="tab-pane active" id="tab-general">

			<div class="col-md-12">
				<div class="form-group">
					<label class="control-label" >Đi chợ cho ngày {{$date}}</label>
				</div>
			</div>

			<div class="col-md-12">
				<div class="form-group">
					<div class="row">
						<div class="col-md-2">
						</div>
						<div class="col-md-2">
							<input class="form-control" tabindex="1"
							placeholder="Tên Thực Phẩm" type="text"
							name="name" id="name"
							value="">
						</div>
						<div class="col-md-2">
							<input class="form-control" tabindex="1"
							placeholder="Đơn Giá" type="text"
							name="price" id="price"
							value="">
						</div>

						<div class="col-md-2">
							<input class="form-control" tabindex="1"
							placeholder="Số Lượng" type="text"
							name="quantity" id="quantity"
							value="">
						</div>
						<div class="col-md-2">
							<button id="add" class="btn btn-default btn-primary">Thêm</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
	{!!Form::open(['action' => 'Admin\FuelDailyController@postCreate'])!!}
	<input type="hidden" value="{{$date}}" name="date">
	<div id="list" class="row" style="height:400px;">
		<div class="col-md-2"></div>
		<div class="col-md-10">
			<table class="table">
			    <thead>
			      <tr>
			        <th>Tên Thực Phẩm</th>
			        <th>Đơn Giá</th>
			        <th>Số Lượng</th>
			        <th>Thành Tiền</th>
			        <th>Lựu Chọn</th>
			      </tr>
			    </thead>
			    <tbody id="tablebody">
			    </tbody>
			 </table>

		 </div>
	</div>
	 <div class="row">
	 	<div class="col-md-2"></div>
	 	<div class="col-md-10" id="totalmoney">
	 	</div>
	</div>
	<div class="form-group">
		<div class="col-md-12">
			<button type="reset" class="btn btn-sm btn-warning close_popup">
				<span class="glyphicon glyphicon-ban-circle"></span> {{
				Lang::get("admin/modal.cancel") }}
			</button>
			<button type="reset" class="btn btn-sm btn-default">
				<span class="glyphicon glyphicon-remove-circle"></span> {{
				Lang::get("admin/modal.reset") }}
			</button>
			<button type="submit" class="btn btn-sm btn-success">
				<span class="glyphicon glyphicon-ok-circle"></span> {{
				Lang::get("admin/modal.create") }}
			</button>
		</div>
	</div>
	{!!Form::close()!!}
	

@stop
@section('scripts')

<script type="text/javascript">
	$(function() {
		var totalmoney=0;
		$("#add").click(function() {
			$name = $("#name").val();
			$price = $("#price").val();
			$quantity = $("#quantity").val();
			if($name!=""&&$price!=""&$quantity!="") {
				if($.isNumeric($price)&&$.isNumeric($quantity)) {
					totalmoney=totalmoney+$price*$quantity;
					$tr=$( "<tr>" );
					$( "<td>" ).html($name).appendTo( $tr);
					$( "<td>" ).html($price).appendTo( $tr);
					$( "<td>" ).html($quantity).appendTo( $tr);
					$( "<td>" ).html($quantity*$price).appendTo( $tr);
					$( "<td>" ).html('<button class="deletebutton btn-danger ">Xóa</button>').appendTo($tr);

					$("<input>").attr("type","hidden").attr("name","names[]").attr("value",$name).appendTo($tr);
					$("<input>").attr("type","hidden").attr("name","prices[]").attr("value",$price).appendTo($tr);
					$("<input>").attr("type","hidden").attr("name","quantitys[]").attr("value",$quantity).appendTo($tr);
					$("<input>").attr("type","hidden").attr("name","totals[]").attr("value",$quantity*$price).appendTo($tr);
					$tr.appendTo($("#tablebody"));

					$("#totalmoney").html("Tổng Tiền: " +totalmoney+ " VND");
					$("#name").val("");
					$("#price").val("");
					$("#quantity").val("");
				}else {
					alert("Giá và số lượng phải là số!");
				}
			}else {
				alert("Hãy Điền Đủ Các Trường!");
			}
		});

$('form').submit(function(event) {
			event.preventDefault();
			var form = $(this);
			if (form.attr('id') == '' || form.attr('id') != 'fupload'){
				$.ajax({
					  type : form.attr('method'),
					  url : form.attr('action'),
					  data : form.serialize()
					  }).success(function() {
						  setTimeout(function() {
							  parent.$.colorbox.close();
							  }, 10);
					}).fail(function(jqXHR, textStatus, errorThrown) {
	                    // Optionally alert the user of an error here...
	                    alert("fail");
	                    var textResponse = jqXHR.responseText;
	                    var alertText = "One of the following conditions is not met:\n\n";
	                    var jsonResponse = jQuery.parseJSON(textResponse);
	                    $.each(jsonResponse, function(n, elem) {
	                        alertText = alertText + elem + "\n";
	                    });
	                    alert(alertText);
	                });
				}
			else{
				var formData = new FormData(this);
				$.ajax({
					  type : form.attr('method'),
					  url : form.attr('action'),
					  data : formData,
					  mimeType:"multipart/form-data",
					  contentType: false,
					  cache: false,
					  processData:false
				}).success(function() {
					  setTimeout(function() {
						  parent.$.colorbox.close();
						  }, 10);
				}).fail(function(jqXHR, textStatus, errorThrown) {
                    // Optionally alert the user of an error here...
                    var textResponse = jqXHR.responseText;
                    var alertText = "One of the following conditions is not met:\n\n";
                    var jsonResponse = jQuery.parseJSON(textResponse);
                    $.each(jsonResponse, function(n, elem) {
                        alertText = alertText + elem + "\n";
                    });
                    alert(alertText);
                });
			};
		});


		$('.close_popup').click(function() {
			parent.$.colorbox.close()
		});
	});
</script>
@stop